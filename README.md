# Debian image
This repository is for creating a docker image of debian to install gpg to use a local repository in an offline. The expectation is that this image is created connected the internet with the latest debian with gpg and curl installed to be able to connect to the local repository. the command to create the image is as follows from this directory
> docker build -t localhost:8082/debian:local  

localhost:8082 depends on where the nexus is hosted and port opened for docker operations to take place with nexus